package com.pucminas.calculadora.service;

import org.springframework.stereotype.Component;

@Component
public class Calculadora {
	public double dividir(double a, double b) {
		return a / b;
	}
	
	public double multiplicar(double a, double b) {
		return a * b;
	}
	
	public double raizCubica(double valor) {
		return Math.cbrt(valor);
	}
	
	public double potencia(double base, double expoente) {
		return Math.pow(base, expoente);
	}
}
