package com.pucminas.calculadora.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pucminas.calculadora.service.Calculadora;

@RestController
@RequestMapping("/api")
public class CalculadoraController {
	@Autowired
	private Calculadora calculadora;
	
    @RequestMapping("/dividir")
    public double dividir(@RequestParam("a") Double a, @RequestParam("b") Double b) {
    	return calculadora.dividir(a, b);
    }
    
    @RequestMapping("/multiplicar")
    public double multiplicar(@RequestParam("a") Double a, @RequestParam("b") Double b) {
    	return calculadora.multiplicar(a, b);
    }
    
    @RequestMapping("/raizCubica")
    public double raizCubica(@RequestParam("valor") Double valor) {
    	return calculadora.raizCubica(valor);
    }
    
    @RequestMapping("/potencia")
    public double potencia(@RequestParam("base") Double base, @RequestParam("expoente") Double expoente) {
    	return calculadora.potencia(base, expoente);
    }
}
