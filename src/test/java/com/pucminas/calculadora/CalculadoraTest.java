package com.pucminas.calculadora;

import org.junit.Assert;
import org.junit.Test;

import com.pucminas.calculadora.service.Calculadora;

public class CalculadoraTest {

	@Test
	public void dividirInteiro() {
		Calculadora calculadora = new Calculadora();
		Assert.assertEquals(5, calculadora.dividir(10, 2), 0);
	}

	@Test
	public void dividirDecimal() {
		Calculadora calculadora = new Calculadora();
		Assert.assertEquals(10.228346456692913, calculadora.dividir(25.98, 2.54), 0);
	}
	
	@Test
	public void dividirNumeroNegativo() {
		Calculadora calculadora = new Calculadora();
		Assert.assertEquals(-200, calculadora.dividir(-800, 4), 0);
	}
	
	
	@Test
	public void multiplicarInteiro() {
		Calculadora calculadora = new Calculadora();
		Assert.assertEquals(20, calculadora.multiplicar(10, 2), 0);
	}

	@Test
	public void multiplicarDecimal() {
		Calculadora calculadora = new Calculadora();
		Assert.assertEquals(65.9892, calculadora.multiplicar(25.98, 2.54), 0);
	}
	
	@Test
	public void multiplicarNumeroNegativo() {
		Calculadora calculadora = new Calculadora();
		Assert.assertEquals(-3200, calculadora.multiplicar(-800, 4), 0);
	}
	
	@Test
	public void raizCubica() {
		Calculadora calculadora = new Calculadora();
		Assert.assertEquals(1.709975946676697, calculadora.raizCubica(5), 0);
	}
	
	@Test
	public void potencia() {
		Calculadora calculadora = new Calculadora();
		Assert.assertEquals(27, calculadora.potencia(3,3), 0);
	}
}