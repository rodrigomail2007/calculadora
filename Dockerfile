#docker build -t registry.gitlab.com/rodrigomail2007/calculadora .
#docker push registry.gitlab.com/rodrigomail2007/calculadora

FROM openjdk:8-jdk-alpine
VOLUME /tmp
ADD ./target/calculadora*.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]